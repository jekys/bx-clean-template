<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__); 

$arTemplate = [
	"NAME"=>Loc::getMessage("CSST_TEMPLATE_NAME"), 
	"EDITOR_STYLES" => [
		//'/bitrix/css/main/bootstrap.css',
		//'/bitrix/css/main/font-awesome.css',
	]
];

?>