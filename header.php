<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;
?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			use Bitrix\Main\Page\Asset;
			//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/fix.js"); 
			//Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/styles/fix.css"); 
			//Asset::getInstance()->addString("<link href='http://fonts.googleapis.com/css?family=PT+Sans:400&subset=cyrillic' rel='stylesheet' type='text/css'>"); 
			$APPLICATION->ShowHead();
		?>

		<title><?=$APPLICATION->ShowTitle()?></title>
	</head>
	<body>
		<div id="panel"><?$APPLICATION->ShowPanel();?></div>
			<!--workarea-->